var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var booksRouter = require('./routes/books');
const mongoose = require('mongoose');
const methodOverride = require('method-override');
var app = express();
const jwt = require('jsonwebtoken');
const secretKey = '§[§¶ÌEõô¤9Mh&%,JÅÂñ¶Ö«TòîJºr·#%hÄ¶Õì¢øäü-R&Æ"k.ÆÔu:?«Äm{LD=ÛªîêÌ';
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(methodOverride(function (req, res) {
  if (req.body && typeof req.body === 'object' && '_method' in req.body) {
    // look in urlencoded POST bodies and delete it
    var method = req.body._method
    console.log(method,req.body._method)
    delete req.body._method
    return method
  }
}));
const checkToken = (req, res, next) => {
  const token = req.cookies.token;

  if (token) {
    jwt.verify(token, secretKey, (err, user) => {
      if (err) {
        res.locals.loggedIn = false;
      } else {
        req.user = user;
        res.locals.loggedIn = true;
      }
      next();
    });
  } else {
    res.locals.loggedIn = false;
    next();
  }
};
const db = 'mongodb+srv://'+process.env.USER_LOGIN+':'+process.env.USER_PASSWORD+'@cluster0.payblz4.mongodb.net/Library?retryWrites=true&w=majority&appName=Cluster0';
mongoose.connection.on('connected', () => console.log('Connected to database.'));
mongoose.connect(db);
app.use(checkToken);
app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/books',booksRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
