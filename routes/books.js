const mongoose = require('mongoose');
require('mongoose-schema-jsonschema')(mongoose);
require('../models/book');
var express = require('express');
var router = express.Router();
const Book = mongoose.model('Book');
var validate = require('jsonschema').validate;
var bookJsonSchema = Book.jsonSchema();
bookJsonSchema['additionalProperties'] = false;
const jwt = require('jsonwebtoken');
const secretKey = '§[§¶ÌEõô¤9Mh&%,JÅÂñ¶Ö«TòîJºr·#%hÄ¶Õì¢øäü-R&Æ"k.ÆÔu:?«Äm{LD=ÛªîêÌ';
const authenticateJWT = (req, res, next) => {
  const token = req.cookies.token;

  if (token) {
    jwt.verify(token, secretKey, (err, user) => {
      if (err) {
        return res.sendStatus(403);
      }

      req.user = user;
      next();
    });
  } else {
    res.sendStatus(401);
  }
};
router.get('/getBooks',async (req,res,next) =>{
  let books = await Book.find();
 res.send({"books":books});
});
/* GET books listing. */
router.get('/', async (req, res, next) => {
  if(res.locals.loggedIn){
    res.render('books', {user: req.user.login, userLoggedIn: res.locals.loggedIn, books: await Book.find(), hideResult: true });
  }else{
    res.redirect('/');
  }
});

router.get('/add', async (req, res, next) => {
  if(res.locals.loggedIn){
  res.render('addBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, hideResult: true });
  }else{
    res.redirect('/');
  }
});

router.get('/delete', async (req, res, next) => {
  if(res.locals.loggedIn){
  res.render('deleteBooks', {user: req.user.login, userLoggedIn: res.locals.loggedIn, hideResult: true });
  }else{
    res.redirect('/');
  }
});

router.get('/edit/:_id', async (req, res, next) => {
  if(res.locals.loggedIn){
  const { _id } = req.params;
  const book = await Book.findOne({ _id });
  res.render('editBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, book: book, hideResult: true });
  }else{
    res.redirect('/');
  }
});
// POST endpoint
router.post('/addBook', authenticateJWT, async (req, res) => {
  if(res.locals.loggedIn){
  req.body['pages'] = Number(req.body['pages']);
  if (req.body['year']) {
    req.body['year'] = Number(req.body['year']);
  }
  let validationResult = validate(req.body, bookJsonSchema);
  if (!validationResult.valid) {
    res.render('addBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Błędne dane książki" });
  }
  const { title, author, pages, year } = req.body;
  const book = new Book({ title, author, pages, year });
  try {
    await book.save();
    res.render('addBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: true, message: "Pomyślnie dodano książkę" });
  } catch (error) {
    res.render('addBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Błąd przy dodawaniu książki" });
  }
}else{
  res.send('Użytkownik nie jest zalogowany');
}
});
router.put('/editBook', async (req, res) => {
  if(res.locals.loggedIn){
  const { _id, title, author, pages, year } = req.body;
  try {
    req.body['pages'] = Number(req.body['pages']);
    if (req.body['year']) {
      req.body['year'] = Number(req.body['year']);
    }
    let validationResult = validate(req.body, bookJsonSchema);
    if (!validationResult.valid) { throw new Error; }
    await Book.findOneAndUpdate({ _id }, { title, author, pages, year });
    const book = new Book({ title, author, pages, year });
    res.render('editBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: true, message: "Pomyślne edytowanie książki", book: book });
  } catch (error) {
    const book = await Book.findOne({ _id });
    res.render('editBook', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Błąd przy edytowaniu książki", book: book });
  }
  }else{
    res.send('Użytkownik nie jest zalogowany');
  }
});
router.delete('/deleteBooks', async (req, res) => {
  if(res.locals.loggedIn){
  const query = req.body;
  const conditions = Object.keys(query)
    .reduce((result, key) => {
      if (query[key]) {
        if (key == 'pages' || key == 'year') {
          result[key] = Number(query[key]);
        } else {
          result[key] = query[key];
        }
      }
      return result;
    }, {});
  try {
    await Book.deleteMany(conditions);
    res.render('deleteBooks', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: true, message: "Pomyślne usuwanie książek" });
  } catch (error) {
    res.render('deleteBooks', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Błąd przy usuwaniu książki" });
  }
}else{
  res.send('Użytkownik nie jest zalogowany');
}
});
module.exports = router;
