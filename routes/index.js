var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res, next) {
  if(res.locals.loggedIn){
  res.render('index', {user:req.user.login, userLoggedIn: res.locals.loggedIn, hideResult: true });
  }else{
    res.render('index', {łuserLoggedIn: res.locals.loggedIn, hideResult: true });  
  }
});

module.exports = router;
