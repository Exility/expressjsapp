const mongoose = require('mongoose');
require('mongoose-schema-jsonschema')(mongoose);
require('../models/user');
require('dotenv').config();
var express = require('express');
var router = express.Router();
const User = mongoose.model('User');
var validate = require('jsonschema').validate;
var userJsonSchema = User.jsonSchema();
userJsonSchema['additionalProperties'] = false;
const md5 = require('md5');
const nodemailer = require("nodemailer");
const accountSid = process.env.TWILIO_ACCOUNT_SID;
const authToken = process.env.TWILIO_AUTH_TOKEN;
const jwt = require('jsonwebtoken');
const secretKey = '§[§¶ÌEõô¤9Mh&%,JÅÂñ¶Ö«TòîJºr·#%hÄ¶Õì¢øäü-R&Æ"k.ÆÔu:?«Äm{LD=ÛªîêÌ';
const client = require('twilio')(accountSid, authToken);
const transporter = nodemailer.createTransport({
  host: "smtp.ethereal.email",
  port: 587,
  secure: false, // Use `true` for port 465, `false` for all other ports
  auth: {
    user: "laurine99@ethereal.email",
    pass: "s6hhvzF3TVN3cTJ3nr",
  },
});

/* GET users listing. */
router.get('/register', async function (req, res, next) {
  if (res.locals.loggedIn) {
    res.redirect('/');
  } else {
    res.render('register', {userLoggedIn: res.locals.loggedIn, hideResult: true });
  }
});

router.get('/logout', (req, res) => {
  if (res.locals.loggedIn) {
    res.clearCookie('token');
    res.redirect('/');
  } else {
    res.redirect('/');
  }
});

router.get('/check-token', (req, res) => {
  const token = req.cookies.token;

  if (token) {
    jwt.verify(token, secretKey, (err, user) => {
      if (err) {
        return res.sendStatus(403); // Token jest nieprawidłowy lub wygasł
      }

      res.json({ loggedIn: true, user }); // Token jest ważny
    });
  } else {
    res.json({ loggedIn: false }); // Brak tokena
  }
});

router.get('/changePassword', (req, res, next) => {
  console.log(req.user.login);
  if (res.locals.loggedIn) {
    res.render('changePassword', {user: req.user.login, userLoggedIn: res.locals.loggedIn, hideResult: true })
  } else {
    res.redirect('/');
  }
});

router.post('/changeUserPassword', async (req, res, next) => {
  const { newPassword, repeatNewPassword } = req.body;
  if(newPassword == repeatNewPassword){
    try{
    await User.findOneAndUpdate({login:req.user.login},{password:md5(newPassword)});
    res.clearCookie('token');
    res.render('index', { userLoggedIn: false, positiveResult: true, message: 'Pomyślnie zmieniono hasło'})
    }catch(error){
      res.render('changePassword', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: false, message: 'Błąd przy zmianie hasła'})
    }
  }else{
    res.render('changePassword', {user: req.user.login, userLoggedIn: res.locals.loggedIn, positiveResult: false, message: 'Hasła nie są identyczne'})
  }
});

router.get('/activate/:_id', async function (req, res, next) {
  const _id = req.params._id;
  await User.findByIdAndUpdate({ _id }, { isActive: true });
  res.send('Użytkownik aktywowany');
});

router.post('/registerUser', async function (req, res, next) {
  const { login, email, repeatEmail, telephone, password, repeatPassword, activationForm } = req.body;
  if (email != repeatEmail || password != repeatPassword) {
    res.render('register', {userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Błednie powtórzono email/hasło" });
  }
  const userExisting = await User.findOne({ login });
  if (userExisting) {
    res.render('register', { userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Podany login jest już zajęty" });
  }
  try {
    const hashedPassword = md5(password);
    const user = new User({ login, email, telephone, password: hashedPassword });
    await user.save();
    if (activationForm == 'Email') {
      await transporter.sendMail({
        from: '"Books📙" <laurine99@ethereal.email>', // sender address
        to: user.email, // list of receivers
        subject: "Hello " + user.login, // Subject line
        html: `<b>Here is <a href="https://expressjsapp-9inv.onrender.com/users/activate/${user._id}">link</a> for activate your account</b>`
      }); // html body
    } else {
      client.messages
        .create({
          body: `Link aktywacyjny:https://expressjsapp-9inv.onrender.com/users/activate/${user._id}`,
          from: process.env.TWILIO_PHONE_NUMBER,
          to: user.telephone
        });
    }
    res.render('index', {userLoggedIn: res.locals.loggedIn, positiveResult: true, message: "Prawidłowo zarejestrowano użytkownika" });

  } catch (error) {
    console.log(error);
    res.render('register', {userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Problem z rejestracją" });
  }
});

router.post('/login', async function (req, res, next) {
  if (!res.locals.loggedIn) {
    const { login, password } = req.body;
    const user = await User.findOne({ login });
    let hashedPassword = md5(password);
    if (user) {
      
      if (user.password == hashedPassword) {
        if (user.isActive) {
          const token = jwt.sign({ login }, secretKey, { expiresIn: '1h' });

          res.cookie('token', token, { httpOnly: true });
          res.redirect('/');
        } else {
          res.render('index', {userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Użytkownik nie aktywowany" });
        }
      } else {
        res.render('index', {userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Podano błędne hasło" });
      }
    } else {
      res.render('index', {userLoggedIn: res.locals.loggedIn, positiveResult: false, message: "Błedny login" });
    }
  } else {
    res.send('Użytkownik jest zalogowany');
  }
});

module.exports = router;
