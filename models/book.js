const mongoose = require('mongoose');
const bookSchema = new mongoose.Schema({
    title: {type:String,required:true},
    author: {type:String,required:true},
    pages: {type:Number,required:true, min:50, max:1000},
    year: {type:Number,min:1800, max:2024}
},
{
    collection:'books',
    versionKey: false
});
mongoose.model('Book', bookSchema);

