const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
    login: { type: String, required: true, unique:true },
    email: { type: String, required: true },
    telephone:{type: String,required:true},
    password: { type: String, required: true },
    isActive: { type: Boolean, required: true, default: false }
},
    {
        collection: 'users',
        versionKey: false
    });
mongoose.model('User', userSchema);

